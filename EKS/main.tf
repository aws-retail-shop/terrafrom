provider "aws" {
  region = var.region
  profile = var.aws_profile
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = var.vpc_name
  cidr = "10.0.0.0/16"

  azs             = ["var.region-a", "var.region-b", "var.region-c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = true

  tags = {
    Terraform = "true"
  }
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.20.0"
  cluster_name    = "retail-shop-cluster"
  cluster_version = "1.28"
  cluster_endpoint_public_access  = true

  cluster_addons = {
    coredns = {
      most_recent = true
    }
    kube-proxy = {
      most_recent = true
    }
    vpc-cni = {
      most_recent = true
    }
  }

  vpc_id                   = module.vpc.vpc_id
  subnet_ids               = module.vpc.private_subnets
  control_plane_subnet_ids = module.vpc.public_subnets

# EKS Managed Node Group(s)
  eks_managed_node_groups = {
    cluster-ng-1 = {
      name = "cluster-ng-1"
      min_size     = 1
      max_size     = 3
      desired_size = 2

      instance_types = ["t3.small"]
    }

    cluster-ng-2 = {
      name = "cluster-ng-2"
      min_size     = 1
      max_size     = 3
      desired_size = 1

      instance_types = ["t3.medium"]
    }
    
    tags = {
    Terraform   = "true"
  }
}
}
