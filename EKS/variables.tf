variable "region" {
  description = "AWS region"
  type        = string
  default     = "ap-southeast-1"
}

variable "aws_profile" {
  description = "AWS Profile"
  type        = string
  default     = "eks-isat"
}

variable "vpc_name" {
  description = "EKS VPC Name"
  type        = string
  default     = "retail-shop-eks-vpc"
}